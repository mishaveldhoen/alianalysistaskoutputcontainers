#ifndef ALIANALYSISTASKTESTOUTPUTCONTAINERS_H
#define ALIANALYSISTASKTESTOUTPUTCONTAINERS_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. * 
* See cxx source for full Copyright notice */ 
/* $Id$ */

#include "TList.h"
#include "TTree.h"
#include "TH1F.h"
#include "TGraph.h"
#include "AliAnalysisTaskSE.h"

class AliAnalysisTaskTestOutputContainers : public AliAnalysisTaskSE {
	
public:
	AliAnalysisTaskTestOutputContainers();
	AliAnalysisTaskTestOutputContainers(const char* name, Int_t testCase);
	~AliAnalysisTaskTestOutputContainers();

	void 						UserCreateOutputObjects();
	void 						UserExec(Option_t*);
	void						Terminate(Option_t*);

private:
	Int_t						fTestCase; // More explanation in run macro.
	TList*						fListOut; //!
	TTree*						fTreeOut; //!
	TH1F*						fHist; //!

	Int_t 						fEventIndex;
	TGraph*						fResMem; //
	TGraph*						fVirMem; //

	ClassDef(AliAnalysisTaskTestOutputContainers, 1);

};

#endif
