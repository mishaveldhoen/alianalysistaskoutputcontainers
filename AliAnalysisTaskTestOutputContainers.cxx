/************************************************************************* 
* Copyright(c) 1998-2008, ALICE Experiment at CERN, All rights reserved. * 
*                                                                        * 
* Author: The ALICE Off-line Project.                                    * 
* Contributors are mentioned in the code where appropriate.              * 
*                                                                        * 
* Permission to use, copy, modify and distribute this software and its   * 
* documentation strictly for non-commercial purposes is hereby granted   * 
* without fee, provided that the above copyright notice appears in all   * 
* copies and that both the copyright notice and this permission notice   * 
* appear in the supporting documentation. The authors make no claims     * 
* about the suitability of this software for any purpose. It is          * 
* provided "as is" without express or implied warranty.                  * 
**************************************************************************/

// -----------------------------------------------------------------------
//  Analysis task testing the memory behavior when connected to different
//  output containers.
// -----------------------------------------------------------------------
//  Author: Misha Veldhoen (misha.veldhoen@cern.ch)

#include <iostream>
#include "AliAODEvent.h"
#include "AliAnalysisTaskTestOutputContainers.h"
#include "TObjectTable.h"
#include "TSystem.h"
#include "TCanvas.h"
#include "TLegend.h"

using namespace std;

ClassImp(AliAnalysisTaskTestOutputContainers);

// -----------------------------------------------------------------------
AliAnalysisTaskTestOutputContainers::AliAnalysisTaskTestOutputContainers():
	AliAnalysisTaskSE(),
	fTestCase(0),
	fListOut(0x0),
	fTreeOut(0x0),
	fHist(0x0),
	fEventIndex(0),
	fResMem(0x0),
	fVirMem(0x0)

{
	
	// Default Constructor
	
}

// -----------------------------------------------------------------------
AliAnalysisTaskTestOutputContainers::AliAnalysisTaskTestOutputContainers(const char* name, Int_t testCase):
	AliAnalysisTaskSE(name),
	fTestCase(testCase),
	fListOut(0x0),
	fTreeOut(0x0),
	fHist(0x0),
	fEventIndex(0),
	fResMem(new TGraph(100000)),
	fVirMem(new TGraph(100000))

{
	
	// Named Constructor
	switch (fTestCase) {
		case 1:
		case 2:
			DefineOutput(1, TTree::Class());
			break;
		case 5:
		case 6:
			DefineOutput(1, TList::Class());
			break;
		default:
			break;
	}
	
	fResMem->SetMarkerStyle(20);
	fResMem->SetMarkerColor(kRed);
	
	fVirMem->SetMarkerStyle(21);
	fVirMem->SetMarkerColor(kBlue);
	fVirMem->SetTitle("Memory Usage");
	(fVirMem->GetXaxis())->SetTitle("N_{event}");
	(fVirMem->GetYaxis())->SetTitle("GB");

}

// -----------------------------------------------------------------------
AliAnalysisTaskTestOutputContainers::~AliAnalysisTaskTestOutputContainers() {
	
	// Destructor
	if (fListOut) {delete fListOut; fListOut = 0x0;}
	if (fTreeOut) {delete fTreeOut; fTreeOut = 0x0;}
	if (fHist) {delete fHist; fHist = 0x0;}

}

// -----------------------------------------------------------------------
void AliAnalysisTaskTestOutputContainers::UserCreateOutputObjects() {

	// Add a branch with the histogram.

	if (fTestCase == 6) {fHist = new TH1F("fHist", "fHist", 10, 0., 10.);}
	
	switch (fTestCase) {
		case 1:
		case 2:
			fTreeOut = new TTree("outputTree", "outptTree");
			if (fTestCase == 2) {fTreeOut->Branch("histogram", "TH1F", &fHist);}
			PostData(1, fTreeOut);
			break;
		case 5:
		case 6:
			fListOut = new TList();
			fListOut->SetOwner(kTRUE);
			if (fTestCase == 6) {fListOut->Add(fHist);}
			PostData(1, fListOut);
			break;
		default:
			break;

	}
	
}

// -----------------------------------------------------------------------
void AliAnalysisTaskTestOutputContainers::UserExec(Option_t*) {

	AliAODEvent* aodEvent = dynamic_cast<AliAODEvent*>(InputEvent());
	
	// cout << "NTracks in event: " << aodEvent->GetNTracks() << endl;
	
	if (fTestCase == 6) {
		for (Int_t iTrack = 0; iTrack < aodEvent->GetNTracks(); iTrack++) {			
			AliAODTrack* track = aodEvent->GetTrack(iTrack);
			fHist->Fill(track->Pt());
		}
	}
	
	switch (fTestCase) {
		case 1:
		case 2:
			PostData(1, fTreeOut);
			break;
		case 5:
		case 6:
			PostData(1, fListOut);
			break;
		default:
			break;
	}
	
	//aodEvent->Reset();
	//gObjectTable->Print();

	// Fill the memory consumption graphs.
	fEventIndex++;
	ProcInfo_t procInfo;
	gSystem->GetProcInfo(&procInfo);
	fResMem->SetPoint(fEventIndex, fEventIndex, (Double_t)procInfo.fMemResident/1000000.);
	fVirMem->SetPoint(fEventIndex, fEventIndex, (Double_t)procInfo.fMemVirtual/1000000.);

}

// -----------------------------------------------------------------------
void AliAnalysisTaskTestOutputContainers::Terminate(Option_t*) {

	TCanvas* cvs = TCanvas::MakeDefCanvas();
	fVirMem->Draw("ap");
	fResMem->Draw("p same");

	TLegend* legend = new TLegend(0.7,0.7,0.95,0.9);
	legend->AddEntry(fResMem, "Res Mem", "p");
	legend->AddEntry(fVirMem, "Vir Mem", "p");
	legend->Draw();

	cvs->SaveAs("MemUsage.root");

}
